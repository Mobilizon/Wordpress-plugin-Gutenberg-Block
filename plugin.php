<?php
/**
 * Plugin Name: Mobilizon Block
 * Plugin URI: https://info.graz.social/mobilizon-block
 * Description: Mobilizon Block is a Gutenberg Block which can display events from Mobilizon.
 * Author: André Menrath
 * Author URI: https://graz.social/@linos
 * Version: 0.3
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package linos
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';

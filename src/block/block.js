/**
 * BLOCK: mobilizon
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
import * as MobilizonWrapper from './../mobilizon-helper';
//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InspectorControls } = wp.blockEditor;
const { Placeholder, PanelBody, PanelRow, CheckboxControl, RangeControl, ColorPicker, TextControl } = wp.components;
 

// Custom icon for the block
const el = wp.element.createElement;
const iconEl = el('img', { class: "wp-block-cgb-mobilizon-icon",  src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAXfklEQVR42u2dB3gVVRbHAyYUUYoLgiI9oqQ3QnrvkS4qUqQGRSywtCglghKKYERaQDoBpVhQ2bVQVjeKIKLLigULKIIIWIGQ5A1n73/ePPcZg5+QuW/mzTvn+/4fsKv4Ju//m3vuuefe6+XFwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHx50HkVYs29rmCdiZ5U3G4D8uDBQ/AC8IT1je99rD8CuCo1iOOl6KVYHAY/48P69+C1vv507rAQJYHCx4QXviDP6wAAhV41YZ++/N6/xRlg/9TFzb4lwodETonVCFUzvJI4bsv07xQCm/AIxfzj9sNZ7/9/tlOCeIBdwpRdVLWszxVF/HETnimOi+5nfmVEr+CKg9nEw9us7FYUImf4vg9fOHslfI1fo+4HQTOQ5aW7jgeqFz8+YJ4UGKxLi6/CwIEpEfqn39a2WmhsFKtqt4y74TX/iFrOb35L+Ctz18s6xKF0eBC2Ro/Orqg4wx4Sp0TmHlirFV7apWtvTnJOeXhL5NVAwjo1LKb6f2ZHbJVCKqpKJrm7a/9trb44LscaQ9/iayaCOnQmVWd6LOiG98R3vJRITDjKOB4+59Z2Sn9t+oO5/wsfeYE9M3CjrRrSpue8Nh7xeE+pgNgZ4EKwBXn1/ot4tSHpafgJaRBu6e3XwuPaV4zXfqDya9PxTq/3VptnwFg6QbA2dWdaP/M9h8Ij10JrxWYqSLkePuXPtquja3E/xuthMUAsPSaB9jEi5UOzOlw7PHBLfzgteI8E6VBGwv86+Dtf2BW+wiR959nAFh6V4MwDzhU5Ht+dV7LZHht/n2+dU0DwDb7h6nz7jTfaK2/gyrXdVL4i2PpIXgJnjpU1LGyeMh1GfDaykFt6zkWyIyOWtqHqfd2QYcYQWqFsp4B+J2eCSbbxkiybYkl2/OJQklOEn9+LoFsm6PJ9mwY2TYE8s/rIgB8+sSNlQsHXZcFr83rc0N9swFQf+fD7eMYAE0bw8m2NZNsr/Ul25ujybZvDtkOriLl8+dJ+fJlJ20l5dAmsh1YQrZ3JpNtx3CyvdJNABHDMFQDwJMDWuTAa6YCQPswV76R3y7eswEIEG/zeLLtvIeUA8WkHHublDPHSbFVkqIof03lZ0k5+R9SPnuGFADxYrrHg+AMwBP9W+TCawVdr7/SLADU1gBo8OqEdgkeCwBMuq0nKV9vJ+WXr4XpK/666auVjZSyH0n59i2ylY4X6VE4AyAAmNuv5S3wmgZAbQbANCmPyPH3zxPGt9XQ+FWEv+9YKdlezmUAzAqA9mE8GwCRqignP9TX/A5ViLTorTEemwo5AzCnb/Ou8NrYjOYNzAJALQcA28a1TfRIAGDMHcNIOfu9HACgj1aIUSaCAbADcJUGQC0zAXCVxwLwTAjZ9j1Oyvmf5QHw3btk2xLHADAAJtSzYaQc2kJKZZk8AMpOk+2FFAaAATDjBDiClKNvXlq58zImw7ZtPUS6FcAAMAAm0+YYUk68L8/8mmzbhwgAghgABsBkej5RXgXIGYBdo+zzDQaAATAXAEmuAeDNBxgABsCE2hJHyvf75QOwc6S9sY4BYADMtgqsHH/X3r4gE4DXB/IcgAEwYxk0nJSvtpFSWS6xClShtUNwFYgBMN1CWCgpB5aSUn5GHgC/HrPvH+B1AAbAfK0QIi15azQp507JA+Dr7fZNMwwAA2DKfQCvdCPlxy/kAfDBfPuOMQaAATBnJSjW3g4hw/xnT5KyI88j838GwJ0a4rAT7Ox3YjJ8Xifz2+zzisP/sO8M4/0ADICptSmSFMwFvtlh3wp5uc1x6ClCZ+nPX5Hy6XqyvdrXI+v/DIDblkUjyPbGYFIOriTl2zdJ+eEzMUE+efGRAdsny34Qhj9s7yn6/AVS9kwn28tdPTbtYQCsUh16IZlsr/cnG3Z07ZlGtn2z1a2Ttg+etAu/3/sY2UonqJtq1BMhPHj/LwPgKWBsCNYUxD8PBoDFYgBYLAaAxWIAWCwGgMViAFgsBoDFADAALAaAAXA/VZT4U9naADqzJoB+WRVIP64UWnERif/vZ/HPnFkdQOfEv1O+jtsgGAA3NPxpYeYvFobS3tkR9I8pXWj5g7E0a1gCTbgzmfJ6pNLAW9Kr1ZBuafTgbSk0fXAiLRwVR5smRtM7MyPo4yfD6NjSYDq/jtuhGQCT6lfx1j5YFEqbhWlnDEmkkb1TqWtKFiXE5lBcdC7FOhT1F6T9s/j3MhOz6a6u6ZTfL4lWCJB2PdqZjhUHq6AxAAyA4W/7E8uC6fmHouih/knqWzw9IVs1cYwEJcTkUI+0TBWuZffHipEhVB0VKhkABsCVOivy+YPCfItFmgLTpwnT420dE+U6JcXlUE8BQ8FdiVRaGEGnlgdRZQkDwABIFCal3ywOphcfjqKh3dMMMb6zMNIkihTr1owMeuLuePpwXpgKJwPAAOiuM6sD6dWCSBojJqkp8TmGmf7P1Dcrg9b+PYaOLA5mABgA/d76ny8IpYX3xlP31ExTGt9ZGJXG9U2mt2dGqOXUSgaAAbhcYYL5/uPhNFYYKj0x2/Tm/22yLNKi/rnp9PLkKPphRSADwABcTsoTQDunR1K/nAxD8/yazA+ykrKpSMwNvn86iAFgAC6tyrNxQjTdIXJqdzN+VWG+gkW4r914XsAAuFBoW9iSH60uYsVG57o9AHYIsumxIYl0tDiIAWAA/vzNj0UtTHZjo6xhfmcIJg1IdMs5AQPgomrPjmmRdEd2hmXe/FWFNYPF98W5HQQMgOwfcIk/fbEgVG1Gi7Oo+R3KTc4SKV6U2p3KADAAqk4sC1I7MK1s/N8tmIlR7q0Znd2moY4BkFnuFG/CkrExately0qU0fZ0BP08yUL4fXyM61aX48V//+93JNORRSFusVDGAMj6wQrtnRNOg7qmS8/7YfDspCx1jjGiVyrl90+iGUMTac7wBHX0gSHv6ppGvdIzKVVMWGV/HkAH8LFJhwHwUABOPm1PfWS+feNjcqlPZgYVCrO/MrkLHV4cTOfXVv95fhJmREPb2jExNPr2FHUhSyYEaKLbMyvc9KkQAyCpzQFVH0wKZfblTB6QRP+eEaFudfyr6QYMeWRxiNp1OiBX3uiEUQDt1KeXBzIAngbA8aXBqjljJG5eWSPe5Ni9dbl9+o5eJIwGsqpTPUXKtf2RSAbAkwCAsbBXV1aDm73UGK3LpnaMGtgPPG1QIiVKSNUwuqB79KSJ+4UYAL1z/+VBlN8vWX1L620oQIXFptM6Ljap6xQLQ2hEz1Qp8xVss0Tjn1l3lDEAOgs1cHzpMsqLEwVYR5fIaTzDhvheGfp/bqRXc/MSxFwgiAGwOgBIf1B+TIzL0b39WK2qzA6X9ibFRHrlgzGUHJctZXFs/9xwU64LMAA66kuRSgzulqZ7sxsWtdBxiQOtZK5bfFQUSsO6p+oOABbknhkfQ+dM2CLBAOgo7JLCgpTeb/8+mZnqoporOlZXjo5VgdM7DcJinBknwwyAjodXzRuRoHtJEZPpR4e4rtX4g7lh6pZHvUeBbqmZ6nEvDIBFAfhKpD8P3Jaiu3FSE7Lp9YJIlx1d+IsAefIA/VewsWdgc340A2BVALAiK+NkB7RRH3rKtW9ObNnM0TmVw0iGKpbZyqEMgA7C5HTd2BgpdfTHUUJc4drc+aOiMOqXo38ahGa8EyabBzAAOggGRZ4uI/9/Lj/K5UeZY76BSavez5OdlE27Z0YwAFYD4NviYMrrmap7YxkW1N4udL1hsI9hyag43dcz0Ir97PhoBsBqAHw2P1RK5yeg+uypUEMW9DDxRsep3vsWcIyKmeYBDIAO+tdjnXWvnUMT+yUZUjuHQTEP0HtNA0JqhdtqGAALAYDFI72b35B/zx6ecNENLq5I62Qc3oWqlhGjGgMgUej/we4svevmuP7IqGfCkYejJaxr3J6FnqYIBsBKAMipmGTR5vwow54JlaBHJZxm0S01i3ZMj2QArARAfwlbC3FDyxsG7qZCd+ii++Kk7GnAdkwGwCIAYI9tbnK2lFThHQNr5miMWz8uRsp2TpwYUckAWAMApAqpCfoDgFHlg3nhhj0XDvJ9aVIXKds6l4iRpWxtAAPg7gCgXHh4UYg6YdXbJIMNrpaUr/OnHdM6SwEAXbNmOTOIAaghADBpsgQAhvVIU48vMfLZsAotAwAshv20kgGwBAD/LQoTAOi/CIZN6jj2xMjne292uJRzgx5T9zcEMQBWAABn6yRLWAW+u1eqerCukc+HZ0uI0R8AHJhllt1hDEANK0C7Z0VIaYO4RwBgtEkAAE540/vZJg1IMhxuBkAnAP5dKAeAkb1TXb4PoKpwkoOM+Q02xhxfygBYAgDcmSsLgFMGn6WzX4wAMp7tof5J9B2PANYA4L054dJSoO8tmgJNGZhkmutVGYAaToL/My9MziS4Z6rhacI+AbeMg3NxbPyp5QyAJQD4dL6cdYDhPdLomyXGlUHRqrB7VriUdYDCoQn04wpeB7AEAF9KWglG3/znC0IMTe9wXqgMAB4fnqA22zEA7g6A0HfLgtW9rnqbZOAt6fTfIuNaIRzHvMsA4KmR8WqzHQNggWY4NHXJuASvb3Y67Z1tXDMczvHcNDFKymnRq8bEmubqJAZAB92WlaH7gbi9MzJop0hBjDvqMZCefjBW/4Ny43No80TznAzBAOigUbfqf83QLSlZtNXAjSPYuI5bJmXccPNaAe8IsxQAjwzSf08wdk7htDmjngllSqzYxkgY2Upn8J5gSwGw4N443U+FwN83X0wWjdo5hTWIod3TpGz0+XBuGANgJQBenRopZcV06l1J6v2+RlS3cNmHjMO+Rt2aapo+IAZAr8NknwylDAmVoPv6pNCRRa5fC8BZpKWFEbrfdIl50tS7El121DsD4CJ9vThYrdvrvXkEd2uhH8eIEuh6Mf/Qe6MPeqaW3R/HZ4NaDQD07aPHXe9KEBbYkF65+ixNrNJi04re8xqsl7xhsouzGQCdrkdaKt5set8PgLUF/L2u3kB+RIxoQ7rrf9kfrkk69FQIA2A1APCG3j4tkrIkzANw6pyrm+Lwltb7rmOMjsMEVGY5DoUB0Fkfi4lwXg/9rxjtmpKp3g/sqtYBHIfyxN3xurd4I/9feG883xFmVQDQ3otDcvWeCKO8+vQDsfTLatekQYcXBdM9vVN1T39QJXtrRmcGwKoAIA3C7Sd6XyoBoIa76IwgjDLbpnShHJ3r/3gGVMmOLglmAKwKAHTgiTC6U8LlcqjHrxkjvy0CdwJgv66M80CL7ok3zR4ABkDigbIPD0iSclkG2hJkToaxOPVqQaTu16NCGFHQ2VpR4s8AWBkACB2cORJaCFLic2jp/bHSNpLgjFN0tco4CQ6VrMOLQkz5fTEAEprIZLRHO+7Z/efULrqXEpGa4LhCGZv7cXL2pgnRpmp/YAAkp0HPSJgMO1IhnBmKnWKVOpofBpWxhoFKElI3lIjN+n0xAJI6KUf2SpWynzZWmw98OC+MymuYU2PTy/px0eoKrYzPipcAVrKxtsAAeAgAjlEA1wDJuDjD+f6ATROj6cSy4Eu6Sb5SuwgbkGLdQsZ8xTFaoXxrphshGQAXCpO+MbfLmQs4DIZtk2ha2zsrXD1sFsa+WKUFOTjSHXSuPpcfRff2TpVyrLvzwheuQjLL6Q8MgAELY+ipwcRVlskcIKQl5KhHKSLdwA3vewQQ++eGqWkS2qlLCzsL00erF1Pclpmhri7HRudK/Vzj70w2/H4DBsBg4RYUbGuU+aatDggsnCGvB3zY1YU+nFgX/fcdN1zunhlxSakZA2BBABwTYvTWxLjQgEYKpVRcgveLCVd9GQCDjhjEAbp4I8da3PxYAZ88MEltqXCX74cBcNHpcZh44pZ0q5o/VrvW6aOiMFO2PDAABuv0ikC1rVnGMYqGm1/MO3qK+QZulXQn8zMABuwdXjwqTvfTFow2P46G3Dopyu3MzwAYceKagAAXRaOG7+7mR8UJJ1e8JMzvqg07DICbA+BomFs1OkaFwF0nxjgAYEBuOr02NZLOrA5w2++CATBIeGOiXQIbaPTePyBbWEgb2zdZzfnd/XtgAAzuGcIJbDhTSMbRijLy/azELPXUaPT4uMNCFwPgBusE2CuLFWO0NMhuUahJvo82ii350XR6eZDLD+tiACwKgDMI6JvHDYpYL4iLNk+6c2tGhnqtEa42rbTYz50BMFkDHfqHcDIDzubvjtVjg0CIF/9dGH/m0EQ110enaaUVf+YMgDlHg+NLg9VbGguFAXFKm6tGBFz0MeiWNLWfZ/esCHXTjJV/1gyAyUcEHLh1SEw40Vo9TaRHqLtjEwuuZq3pWaRIb7Aoh3Ls4K5pNHdEggod9gy4c2mTAbDal6T1E+Haok/EPAHHlyAnH9c3Wd0Zdmd2ukhXMtWGO5gZLdBVhf8dIwkmsrilBed0Th2YSKtGx6pHlsD0PwjYzHZ2JwPAAFSbIqEEeU6YFe0VuK1+7+wI9YBezB9emfxH4TSJfwmjY6PMV4tCVLNjlxj2FVulomN9ADb4VzIALI8CYHSfG+rjQ708rlW8st6vjAFg6aoSPwWeOjjX9/zMO5rmwmt59peuOQAYlNS2nvi1wZwBzQPK1/l9C1oZAJaeAFSs86M9M9qfGJZ2TRQA6BOtvnRNAUBtf3+vOgBAqNmZVZ32MQAs/apsfiSyCuXXVZ1o15R2B4XHWsJrvr5edeE9UwAg5N3F95qGAODoopvWKBq1/AWy9AJA+Io2j269FR7TvOZtKgDSwps0wofbOr71gPNr/UgdBUr8LvCXyKqZ/C7ghbp/li/N7tv8PngsKaRxYzMBgDzMu3lzNQVqKtT2q/kd3ytb4ydGAf8KEMxfIuvy3/7+FSeKb6IXx7X5SHirAzzWrJnXVRoAtcwCwBVC9YM6XHWt+LVV8dDrh2HIwsSF5wKsmqQ+5cJDpdPa07Tbmt8PbwW0a9AcXtM8ZyoA6jVq5NXkmvpercTvb3xlQuvVX87vSOIBLgACHglYl2p+pD/vFXagp/NaboSn4C14DF4zEwCOeQAqQQ2bXul1vfi1vVDA1nGtX/pk3o10bo19KMMD4eEYBlZ1prf7Qs35KzCPhPlXjmz1GrwET/2tvloBaqh5rbbZAPDRSqFNmzeqBwBuEgpaP6rlhn2FvvTtYntKBLIdQqVIHR1YHiu1Wljyf09gwnt8yU30ZkF7Kh7ecrPwUDC8pHmqqeYxH7NMgP+QBgmhGtSi6dV1O4pf/Xx8fIILbm0xduu4Nh+DaKRFPy6/mUA4yEe1iOW5guGR5/+8ohMdWdCR9jzWgTY92PqT/B7Xjod34CHNSy00b5ku/aluFPib0A1X1/G6uUEdr0A8iFDnSb2a5a+6t9VrL41vc2j7pHan9hV2KN8/u4Pt/ULfSpYHalYHm8gOKnZNaXdq6/jWh1bd0+r1h3o2e1h4JRKegXfgIXhJ85Qp3/5VRwGs0F2Nei1m7XXrenW8trFKMiAIFQrz9vaOzgxt1Dsv/Zphd6c2HcHyTI1IaXp3Xso1w+AFeALegEfgFXgG3oGHNC9drXnLlG//360JaMNUQwcEQr516nj5N2vsE9Ks8ZWAIMQBA4uleUH1hvBIKLwCzziZv6HmKW8zm98BQO0qEGDigspQW5SyhG4WDxjYuIFPSNNGPmHXNfIJF4pgeaTC4QHhhWB4At7QPNJW80zTKuavbXYAqkKAIQurdqjdXqs1MrURaqcRjoftyPJo3ah5oZ3mjZaaV5po3qnrTuavCsEVWs22vpbDNdGoxgNep1HekuXRul7zwrWaN5poXqmveecKdzP/xUaDOtpQ1kAju6FW1mrspCYsj5Dzd95I88JVmjfqaV7xdmfjV4XAeUTw1kpZdbThra720CzPk+P7r6N5wrvKG9/tzX8xEGo7AcFiOXvCcsb/K1CwPFscHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBw1j/8B3ubwo0V0jNIAAAAASUVORK5CYII=" });

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-mobilizon', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Mobilizon' ), // Block title.
	icon: iconEl, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'mobilizon' ),
		__( 'events' ),
	],
	attributes: {
		mobilizonInputField: {
			type: 'string',
			default: ''
		},
		mobilizonInputFieldIsValid: {
			type: 'boolean',
			default: false
		},
		mobilizonInputFieldHelpText: {
			type: 'string',
			default: ''
		},
		mobilizonBaseURL: {
			type: 'string',
			default: ''
		},
		mobilizonGroupName: {
			type: 'string',
			default: ''
		},
		mobilizonEventLimit: {
			type: 'int',
			default: 0
		},
		mobilizonShowHeaderImage: {
			type: 'boolean',
			default: true
		},
		myRichText: {
			type: 'string',
			source: 'html',
			selector: 'p'
		},
		toggle: {
			type: 'boolean',
			default: true
		},
		favoriteAnimal: {
			type: 'string',
			default: 'dogs'
		},
		favoriteColor: {
			type: 'string',
			default: '#DDDDDD'
		},
		activateLasers: {
			type: 'boolean',
			default: false
		}
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: (props) => { 
		const { attributes, setAttributes } = props;
		async function updateAndValidateMobilizonActor(newtext) {
			// Update the input-text-field, so one can see the input while typing 
			setAttributes({ mobilizonInputField: newtext })
			// Assume that any valid string of an Mobilizon actor or instance is more than 7 characters long (May be risky!).
			newtext = newtext.trim();
			if (newtext.length > 7) {
				// Possible Actors: @user@example.org or example.org/@user or http(s)://example.org/@user
				if (newtext.includes("@")) {	
					// Here we may have actor+instance (actor means mobilizon-group)
					// Possible formats could be "@actor@mobilizon.fr" or "https://mobilizon.fr/@actor" or even "mobilizon.fr/@actor"
					var splitted = newtext.split("@");
					if ( splitted.length <= 3) {
						if (splitted.length === 3 && splitted[0] === "") {
							var baseURL = splitted[2];
							var groupName = splitted[1];
						}
						else {
							var baseURL = splitted[0];
							var groupName = splitted[1];
						}
						// Asume that the instance name is at least 6 characters long (may be risky)
						if (baseURL.includes(".") && baseURL.length > 5) {
							// Add https protocol if not present
							if (!baseURL.startsWith("https://")) {
								baseURL = "https://" + baseURL;
							}
							if ( await MobilizonWrapper.isMobilizonInstance(baseURL)) {
								// Now check if the group exists as well
							    if ( await MobilizonWrapper.groupExists(baseURL, groupName) ) {
									setAttributes({ mobilizonBaseURL: baseURL});
									setAttributes({ mobilizonGroupName: groupName });
									setAttributes({ mobilizonInputFieldHelpText: "" });
									setAttributes({ mobilizonInputFieldIsValid: true });
								} else {
									var helperText = "The group " + groupName + " does not exist on " + baseURL;
									setAttributes({ mobilizonInputFieldHelpText: helperText });
								}
							} else {
								var helperText = baseURL + " is not a mobilizon instance";
								setAttributes({ mobilizonInputFieldHelpText: helperText });
							}
						}
					}
				}
				else {
					// Here we only have the instance domain alone, no actor (group)!
					// Instance-URL must have at least one dot, an the after the first dot there must at least be two more characters for a valid domain
					if (newtext.includes(".") && newtext.substring(newtext.indexOf(".")).length > 2) { 
						var baseURL = newtext;
						// Add https protocol if not present
						if (!baseURL.startsWith("https://")) {
							baseURL = "https://" + baseURL;
						}
						if ( await MobilizonWrapper.isMobilizonInstance(baseURL)) {
							setAttributes({ mobilizonBaseURL: baseURL});
							setAttributes({ mobilizonGroupName: "" });
							setAttributes({ mobilizonInputFieldHelpText: "" });
							setAttributes({ mobilizonInputFieldIsValid: true });
						} else {
							var helperText = baseURL + " is not a mobilizon instance";
							setAttributes({ mobilizonInputFieldHelpText: helperText });
						}
					}
				}
			} else {
				var helperText = newtext + " is no valid mobilizon actor"
				setAttributes({ mobilizonInputFieldHelpText: helperText });
			}
		}
		return (
			<div className={ props.className }>
				<InspectorControls>
					<PanelBody
						title="Settings"
						initialOpen={true}
					>
						<PanelRow>
							<RangeControl
								label="Limit how many events are shown (0 means no limit)"
								value={ attributes.mobilizonEventLimit }
								onChange={ ( value )  => setAttributes({ mobilizonEventLimit: value })}
								min={ 0 }
								max={ 20 }
							/>
						</PanelRow>
						<PanelRow>
							<CheckboxControl
								label="Header Images"
								help="Set if the header images for each event are shown in the list?"
								checked={attributes.mobilizonShowHeaderImage}
								onChange={(newval) => setAttributes({ mobilizonShowHeaderImage: newval })}
							/>
						</PanelRow>
					</PanelBody>
				</InspectorControls>
				<Placeholder icon={ iconEl } label="Mobilizon Event List">
					<TextControl 
						tagName="h2"
						placeholder="@example@mobilizon.fr"
						label="Fediverse handle or URL of a Mobilizon-group or Mobilizon-Instance-URL"
						help={ attributes.mobilizonInputFieldHelpText }
						value={ attributes.mobilizonInputField }
						onChange={(newtext) => updateAndValidateMobilizonActor(newtext)}
					/>
				</Placeholder>
			</div>
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		return ( null );
	},
} );

# Mobilizon-Block (Wordpress Plugin)
This Plugin is beta-state. Use it carefully! But you are welcome to report bugs or feature wishes.

## Features
- Easy to use and setup
- Integrates nicely in your existing theme
- Show events of specific group or whole instance
- Configuration in the  block settings:
  - Maximum number of events shown
  - Selectable designs (soon)
  - Amount of event-details displayed (soon)
- Events get cached on the server as transients (soon)


## Differences to connector-mobilizon
This Wordpress-Plugin is inspired by https://github.com/wordpress-connector-for-mobilizon/connector-mobilizon/.
- Using a Gutenberg-Block, not a shortcode
- Fetches the events on the server side (php), not on the client side (JavaScript), which is out-of-the-box GDPR-compatible.

## Changelog
### v0.3 - 04. August 2021
- Add event descriptions in modal box (not optional yet)

### v0.2 - 04. August 2021
- Enhance backend design
- Make setup more fail-proof
- small fixes

### v0.1 - 01. August 2021
- Initial Proof of Concept



# Contribute

## Development
If you want to contribute code, translations or have ideas and so on, please see the [https://codeberg.org/linos/mobilizon-block/issues](issue) section first. You are very welcome.

### 👉  `npm install`
- Fetches the dependencies
- Usually only needed the first time, or when the dependencies are updated

### 👉  `npm start`
- Use to compile and run the block in development mode.
- Watches for any changes and reports back any errors in your code.

## 👉  `npm run build`
- Use to build production code for your block inside `dist` folder.
- Runs once and reports back the gzip file sizes of the produced code.

## Additional Information
This project was bootstrapped with [Create Guten Block](https://github.com/ahmadawais/create-guten-block).